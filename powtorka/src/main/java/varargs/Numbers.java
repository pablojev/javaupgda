package varargs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-06-13.
 */
public class Numbers {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4};

        printNumber(array);


        printNumber(8, 2, 10);
    }


    public static void printNumber(int... numbers) {
        for (int number : numbers) {
            System.out.println(number);
        }

    }
}
