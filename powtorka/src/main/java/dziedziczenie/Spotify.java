package dziedziczenie;

import java.util.Random;

/**
 * Created by RENT on 2017-06-13.
 */
public class Spotify {

    private static Random random = new Random();

    public static Music selectRandomMusic() {
        switch (random.nextInt() % 2) {
            case 0:
                return new Rap();
            case 1:
                return new Techno();
        }
        return new Techno();
    }
}

