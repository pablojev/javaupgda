package dziedziczenie;

/**
 * Created by RENT on 2017-06-13.
 */
public class Player {

    public static void main(String[] args) {

        for(int i = 0 ; i<100 ; i++) {
            Music music = Spotify.selectRandomMusic();

            music.play();
            play(music);
        }
    }

    private static void play(Music music) {
        System.out.println("Gram muzykę");
        music.play();
    }

    private static void play(Rap music) {
        System.out.println("Gram rap");
        music.play();

    }
}
