package sportsman;

/**
 * Created by Adrian on 2017-06-23.
 */
public class FitnessStudion {

    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(2);
        sportsman.doSquats(5);
        sportsman.doCrunches(5);
    }

}
