package sportsman;

/**
 * Created by Adrian on 2017-06-23.
 */
public class SportsmanBuilder {

    private Sportsman sportsman = new BasicSportsman();

    public SportsmanBuilder drinksWater() {
        sportsman = new WaterDrinking(sportsman);
        return this;
    }

    public SportsmanBuilder makesSelfies() {
        sportsman = new SelfieMaking(sportsman);
        return this;
    }

    public SportsmanBuilder dontLikePreparing() {
        sportsman = new NoPrepare(sportsman);
        return this;
    }

    public SportsmanBuilder alwaysDoesDoubleSeries() {
        sportsman = new DoubleSeries(sportsman);
        return this;
    }

    public static SportsmanBuilder sportsmanWho() {
        return new SportsmanBuilder();
    }

    public Sportsman getHim() {
        return sportsman;
    }
}
