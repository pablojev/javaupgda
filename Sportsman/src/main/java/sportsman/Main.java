package sportsman; /**
 * Created by Adrian on 2017-06-23.
 */

import static sportsman.SportsmanBuilder.*;
public class Main {

    public static void main(String[] args) {
        //sportsman.Sportsman sportsman = new sportsman.NoPrepare(new sportsman.WaterDrinking(new sportsman.DoubleSeries(new sportsman.BasicSportsman())));
        //sportsman.Sportsman sportsman = new sportsman.BasicSportsman();

        //Fluent interface
        Sportsman sportsman = sportsmanWho()
                .alwaysDoesDoubleSeries()
                .drinksWater()
                .makesSelfies()
                .getHim();


        FitnessStudion fitnessStudion = new FitnessStudion();
        fitnessStudion.train(sportsman);
    }
}
