package pl.pawel.mtm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by pablojev on 03.08.2017.
 */
@Entity
public class Meal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @JsonIgnoreProperties("meal")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.meal", cascade=CascadeType.ALL)
    private List<MealProduct> mealProductList;

    public Meal() {
    }

    public Meal(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MealProduct> getMealProductList() {
        return mealProductList;
    }

    public void setMealProductList(List<MealProduct> mealProductList) {
        this.mealProductList = mealProductList;
    }
}
