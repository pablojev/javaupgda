package pl.pawel.mtm.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by pablojev on 03.08.2017.
 */
@Embeddable
public class MealProductId implements Serializable {
    @ManyToOne
    private Meal meal;
    @ManyToOne
    @JsonBackReference
    private Product product;

    public MealProductId() {

    }

    public MealProductId(Meal meal, Product product) {
        this.meal = meal;
        this.product = product;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
