package pl.pawel.mtm.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pawel.mtm.entity.Meal;

/**
 * Created by pablojev on 03.08.2017.
 */
public interface MealRepository extends CrudRepository<Meal, Long> {

}
