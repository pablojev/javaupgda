package pl.pawel.mtm.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pawel.mtm.entity.Product;

/**
 * Created by pablojev on 03.08.2017.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
}
