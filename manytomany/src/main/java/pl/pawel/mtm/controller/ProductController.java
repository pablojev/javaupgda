package pl.pawel.mtm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.pawel.mtm.entity.Product;
import pl.pawel.mtm.repository.ProductRepository;

/**
 * Created by pablojev on 03.08.2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping("/add")
    public Product add(@RequestParam("name") String name,
                       @RequestParam("carbon") double carbon,
                       @RequestParam("protein") double protein,
                       @RequestParam("kcal") double kcal) {
        return productRepository.save(new Product(name, carbon, protein, kcal));
    }
}
