package pl.pawel.mtm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.pawel.mtm.entity.Meal;
import pl.pawel.mtm.entity.MealProduct;
import pl.pawel.mtm.entity.MealProductId;
import pl.pawel.mtm.entity.Product;
import pl.pawel.mtm.repository.MealRepository;
import pl.pawel.mtm.repository.ProductRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/meal")
public class MealController {

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping("/add")
    public Meal add(@RequestParam("name") String name) {
        return mealRepository.save(new Meal(name));
    }

    @RequestMapping("/show")
    public List<Meal> showAll() {
        return (List<Meal>) mealRepository.findAll();
    }

    @RequestMapping("/add/meal/{mealId}/product/{productId}/amount/{amount}")
    public Meal addProduct(@PathVariable("mealId") long mealId,
                           @PathVariable("productId") long productId,
                           @PathVariable("amount") double amount) {
        Meal meal = mealRepository.findOne(mealId);
        Product product = productRepository.findOne(productId);

        meal.getMealProductList().add(new MealProduct(new MealProductId(meal, product), amount));
        return mealRepository.save(meal);
    }

}
