package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	public double readDouble() {
		double in = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczbę typu double: ");
		try {
			in = sc.nextDouble();
		} catch(InputMismatchException e) {
			// nic nie rób :)
		}
		return in;
	}
	
	public int readInt() {
		int in = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczbę całkowitą: ");
		try {
			in = sc.nextInt();
		} catch(InputMismatchException e) {
			
		}
		return in;
	}
	
	public String readString() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj łańcuch znaków: ");
		return sc.nextLine();
	}
}
