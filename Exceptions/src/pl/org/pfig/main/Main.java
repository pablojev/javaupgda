package pl.org.pfig.main;

public class Main {

	public static void main(String[] args) throws WrongAgeException {
		
		// Zadanie 2.
		
		try {
			System.out.println("Pierwiastek wynosi: " + Square.square(4));
		} catch(IllegalArgumentException e) {
			System.out.println("Podano nieprawidłowy argument: " + e.getMessage());
		}
		
		// Zadanie 3.
		
		try {
			System.out.println(Division.divide(5.4, 0));
		} catch(IllegalArgumentException e) {
			System.out.println("Wystapil wyjatek: " + e.getMessage());
		}
		
		// Zadanie 4.
		
		ReadNumbers rn = new ReadNumbers();
		System.out.println("Wczytana liczba zmiennoprzecinkowa to: " 
														+ rn.readDouble());

		System.out.println("Wczytana liczba całkowita to: " 
				+ rn.readInt());
		
		System.out.println("Wczytany łańcuch znaków to: " 
				+ rn.readString());
		
		// Zadanie 5.
		
		QuadraticEquation qe = new QuadraticEquation();
		
		double[] res = qe.solve();
		
		for(int i = 0; i < res.length; i++) {
			System.out.println("x" + (i + 1) + " = " + res[i]);
		}
		
		// Zadanie 6.
		
		//throw new WrongAgeException(); // throw new WrongAgeException("tekst");
		
		// Zadanie 7, 8
		
		People p = new People(1);
		
		try {
			p.addPerson(new Person("a", "b",1, "c", "d", 12.5));
			p.addPerson(new Person("aa", "bb",11, "cc", "dd", 112.5));
			p.addPerson(new Person("aaa", "bbb",111, "ccc", "ddd", 1112.5));
		} catch(FullListException e) {
			System.out.println("Nastapil wyjatek");
		}
		
		for(Person person : p.getPpl()) {
			System.out.println(person);
		}
	}

}
