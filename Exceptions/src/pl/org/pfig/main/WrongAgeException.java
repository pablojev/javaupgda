package pl.org.pfig.main;

public class WrongAgeException extends Exception {
	public WrongAgeException() {
		
	}
	
	public WrongAgeException(String message) {
		super(message);
	}
}
