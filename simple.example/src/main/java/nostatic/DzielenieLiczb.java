package nostatic;

import java.util.Scanner;

public class DzielenieLiczb {

	public static void main(String[] args) {

		//ifExample();
		exceptionExample();
	}

	private static void exceptionExample() {
		System.out.println("Podaj dwie liczby");
		Scanner scanner = new Scanner(System.in);

		int a = scanner.nextInt();
		int b = scanner.nextInt();

		try {
			System.out.println("a/b = " + a / b);
		}catch (ArithmeticException e) {
			System.out.println("Nie wolno dzieli� przez zero");
		}
	}

	private static void ifExample() {
		System.out.println("Podaj dwie liczby");
		Scanner scanner = new Scanner(System.in);

		int a = scanner.nextInt();
		int b = scanner.nextInt();

		if (b == 0) {
			System.out.println("Nie wolno dzieli� przez zero");
		} else {
			System.out.println("a/b = " + a / b);
		}
	}
}
