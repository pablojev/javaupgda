package nostatic;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DzielenieLiczb2 {

	public static void main(String[] args) {
		System.out.println("Podaj dwie liczby");
		Scanner scanner = new Scanner(System.in);
		try {
			int a = scanner.nextInt();
			int b = scanner.nextInt();

			if (b == 0) {
				System.out.println("Nie wolno dzieli� przez zero");
			} else {
				System.out.println("a/b = " + a / b);
			}
		} catch (InputMismatchException e) {
			System.out.println("To nie s� liczby");
		}
	}
}
