package simple.example;

public class TabliceWielowymiarowe {

	
	public static void main(String[] args) {
		
		int[][] ints = new int[6][];
		
		System.out.println(ints[2]);
		
		ints[2] = new int[]{1,23,2};
		ints[3] = new int[]{1,2};
		System.out.println(ints[2][1]);
		
		System.out.println(suma(ints[2]));
		
		System.out.println("Sumy : ");
		for(int i : suma(ints)) {
			System.out.println(i);
		}
	}
	
	public static int[] suma(int[][] liczby) {
		int[] results = new int[liczby.length];
		for(int i = 0 ; i < liczby.length; i++) {
			int[] element = liczby[i];
			if (element != null) {
				results[i] = suma(element);
			}
		}
		return results;
	}
	
	public static int suma(int[] liczby) {
		int suma = 0;
		for(int i : liczby) {
			suma += i;
		}
		return suma;
	}
}
