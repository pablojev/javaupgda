package family;

public class Family {

	public static void main(String[] args) {
		Mother mother = new Mother("Gra�yna");
		/*
		 * Gdyby metoda introduce nie by�a statyczna musia�bym zrobi� tak
		 * Family family = new Family();
		 * family.introduce(mother);
		 */
		introduce(mother);
		Father father = new Father("Janusz");
		introduce(father);
		Daugther daugther = new Daugther("Ania");
		introduce(daugther);
		Son son = new Son("Adam");
		introduce(son);
	}
	
	public static void introduce(Mother mother) {
		System.out.println("I�m a mother. My name is " + mother.getName());
	}
	
	public static void introduce(Father father) {
		System.out.println("I�m a father. My name is " + father.getName());
	}
	
	public static void introduce(Son son) {
		System.out.println("I�m a son. My name is " + son.getName());
	}
	
	public static void introduce(Daugther daugther) {
		System.out.println("I�m a daugther. My name is " + daugther.getName());
	}

}
