package family.dziedziczenie;

public class Family {

	public static void main(String[] args) {
		FamilyMember mother = new Mother("Gra�yna", 1000);
		FamilyMember father = new Father("Janusz");
		FamilyMember daugther = new Daugther("Ania", false);
		FamilyMember son = new Son("Adam", false);
		
		FamilyMember[] members = {mother, father, daugther, son};
		
		for (FamilyMember familyMember : members) {
			familyMember.introduce();
			System.out.println(familyMember.isAdult());
		}
	}

}
