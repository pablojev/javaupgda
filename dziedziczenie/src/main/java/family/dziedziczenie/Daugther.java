package family.dziedziczenie;

public class Daugther extends FamilyMember {
	
	public Daugther(String name, boolean adult) {
		super(name, adult);
	}

	@Override
	public void introduce() {
		System.out.println("I�m a daugther. My name is " + this.getName());
	}
}
