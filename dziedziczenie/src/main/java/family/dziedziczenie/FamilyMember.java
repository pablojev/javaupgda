package family.dziedziczenie;

public abstract class FamilyMember {
	private String name;
	private boolean adult;
	
	public FamilyMember(String name, boolean adult) {
		this.name = name;
		this.adult = adult;
	}

	public boolean isAdult() {
		return adult;
	}
	
	public String getName() {
		return name;
	}
	
	public abstract void introduce();
}
