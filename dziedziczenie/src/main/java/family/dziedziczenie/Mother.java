package family.dziedziczenie;

public class Mother extends FamilyMember {
	private int salary;
	public Mother(String name,int salary) {
		super(name, true);
		this.salary = salary;
	}
	
	@Override
	public void introduce() {
		System.out.println("I�m a mother. My name is " + this.getName());
	}
	
	
	public void cokolwiek(){
		
	}
}
