package family.dziedziczenie;

public class Son extends FamilyMember {
	
	public Son(String name, boolean adult) {
		super(name,adult);
	}

	@Override
	public void introduce() {
		System.out.println("I�m a son. My name is " + this.getName());
	}
}
