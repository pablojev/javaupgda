package pl.sdacademy.hiber.crud;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sdacademy.hiber.entity.Movie;
import pl.sdacademy.hiber.util.HibernateUtil;

import java.util.List;

/**
 * Created by pablojev on 13.07.2017.
 */
public class SimpleCRUD<T> implements CRUD<T> {
    private Class<T> clazz;

    public SimpleCRUD(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T select(int id) {
        Session session = HibernateUtil.openSession();
        T obj = (T) session.get(clazz, id);
        session.close();
        return obj;
    }

    public void insertOrUpdate(T obj) {
        Session session = HibernateUtil.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(obj);
        tx.commit();
        session.close();
    }

    public void delete(Object obj) {

    }

    public List<T> select() {
        List<T> ret;
        Session session = HibernateUtil.openSession();
        Transaction tx = session.beginTransaction();
        System.out.println("\t" + clazz.getSimpleName());
        ret = session.createQuery("from " + clazz.getSimpleName()).list();
        tx.commit();
        session.close();
        return ret;
    }
}
