package pl.sdacademy.hiber.crud;

import java.util.List;

/**
 * Created by pablojev on 13.07.2017.
 */
public interface CRUD<T> {
    void insertOrUpdate(T obj);
    void delete(T obj);
    T select(int id);
    List<T> select();
}
