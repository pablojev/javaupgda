package pl.sdacademy.hiber.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by pablojev on 13.07.2017.
 */
public class HibernateUtil {
    private static final SessionFactory sf = new Configuration().configure().buildSessionFactory();

    private HibernateUtil() {}

    public static Session openSession() {
        return sf.openSession();
    }
}
