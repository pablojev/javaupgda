package pl.sdacademy.hiber;

import pl.sdacademy.hiber.crud.SimpleCRUD;
import pl.sdacademy.hiber.entity.Genre;
import pl.sdacademy.hiber.entity.Movie;

import java.util.List;

/**
 * Created by pablojev on 11.07.2017.
 */
public class MovieDB {
    public static void main(String[] args) {

        SimpleCRUD<Movie> cm = new SimpleCRUD(Movie.class);

        Genre g = new Genre("Komedia");

        // zapis
        Movie m = new Movie();
        m.setDescription("super filmik");
        m.setDuration(180);
        m.setTitle("Pierwszy film");
        m.setYear(2000);
        m.setGenre(g);

        cm.insertOrUpdate(m);
        cm.insertOrUpdate(new Movie("Drugi film", 2002, 190, "Opis drugiego filmu", g));


        // odczyt
        Movie mv = cm.select(1);
        System.out.println(mv.getTitle() + " (" + mv.getYear()+ "), gatunek: " + mv.getGenre().getName());

        SimpleCRUD<Genre> sg = new SimpleCRUD(Genre.class);

        Genre newG = sg.select(1);
        List<Movie> lG = newG.getMovie();
        System.out.println("aaa -- " + lG.size());
        for(Movie newM : newG.getMovie()) {
            System.out.println("-- " + newM.getTitle());
        }

        System.exit(0);

    }
}
