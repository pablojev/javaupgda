package pl.org.pfig.cities.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by pablojev on 06.07.2017.
 */
public class DatabaseConnector {
    private static DatabaseConnector _instance = null;
    private Connection _connection;

    private DatabaseConnector(DatabaseConnectorBuilder dcb) throws SQLException {
        String url = "jdbc:mysql://" + dcb.hostname() + ":" + dcb.port() + "/" + dcb.database();
        _connection = DriverManager.getConnection(url, dcb.username(), dcb.password());
    }

    public static DatabaseConnector getInstance() throws SQLException {
        if(_instance == null) {
            _instance = new DatabaseConnector(new DatabaseConnectorBuilder()
                                                    .hostname("localhost")
                                                    .port(8889)
                                                    .username("root")
                                                    .password("root")
                                                    .database("rental"));
        }
        return _instance;
    }

    public Connection getConnection() {
        return _connection;
    }
}
