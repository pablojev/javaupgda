package pl.org.pfig.cities.app;

import pl.org.pfig.cities.connector.DatabaseConnector;
import pl.org.pfig.cities.resolver.AbstractResolver;

import javax.swing.text.WrappedPlainView;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pablojev on 07.07.2017.
 */
public class Application {

    private Map<String, AbstractResolver> _services = null;
    private Connection _connection;

    public Application() throws SQLException {
        this._connection = DatabaseConnector.getInstance().getConnection();
        this._services = new HashMap<>();
    }

    public Application register(String name, AbstractResolver resolver) {
        // TODO : sprawdzenie
        resolver.connect(_connection);
        this._services.put(name, resolver);
        return this;
    }

    public AbstractResolver service(String name) {
        return this._services.get(name);
    }
}
