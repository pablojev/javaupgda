package pl.org.pfig.cities;

import pl.org.pfig.cities.app.Application;
import pl.org.pfig.cities.connector.DatabaseConnector;
import pl.org.pfig.cities.helper.QueryHelper;
import pl.org.pfig.cities.model.City;
import pl.org.pfig.cities.reader.PropertyReader;
import pl.org.pfig.cities.resolver.CityResolver;
import pl.org.pfig.cities.resolver.FileConfigResolver;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pablojev on 06.07.2017.
 */
public class Main {
    public static void main(String[] args) throws SQLException{

        Map<String, String> prop = new HashMap<>();
        prop.put("id", "2");
        prop.put("name", "Brzęszczyszczykiewicz");
        prop.put("lastname", "Tester");
        prop.put("innaklolumna", "innawartosc");
        System.out.println(prop.get("name"));

        System.out.println(QueryHelper.insert("student", prop));


        PropertyReader pr = new PropertyReader();

        System.out.println(pr.getProperty("dbport"));

        System.exit(0);

        Application app = new Application();
        app.register("city", new CityResolver());

        List<City> listOfCities2 = app.service("city").get();

        for(City c : listOfCities2) {
            System.out.println(c.getId() + ". " + c.getCity());
        }

        //System.out.println(QueryHelper.update("student", prop));

        Map<String, String> config = FileConfigResolver.resolve("db");

        for(Map.Entry e : config.entrySet()) {
            System.out.println(e.getKey() + " > = < " + e.getValue());
        }


        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "UPDATE `city` SET `city` = ? WHERE `city` = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(2, "Sieradz");
            ps.setString(1, "Włocaławek");
            int countRows = ps.executeUpdate();
            System.out.println("Nadpisałem " + countRows + " wiersz(y).");
            ps.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "DELETE FROM `city` WHERE `id` = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, 1);
            int countRows = ps.executeUpdate();
            System.out.println("Usunalem " + countRows + " rekord(y/ów)");
            ps.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }


        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();

            s.execute("INSERT INTO `city` VALUES(NULL, 'Wronki')");
            s.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }

        // select
        List<City> listOfCities = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM `city`";

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                listOfCities.add(new City(rs.getInt("id"), rs.getString("city")));
            }

            rs.close();
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        for(City c : listOfCities) {
            System.out.println("> "  + c.getCity());
        }

    }
}
