package pl.org.pfig.cities.resolver;

import pl.org.pfig.cities.model.City;

import java.sql.Connection;
import java.util.List;

/**
 * Created by pablojev on 07.07.2017.
 */
public abstract class AbstractResolver<T> {
    public abstract List<T> get();

    public abstract void connect(Connection connection);
}
