package pl.org.pfig.cities.reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by pablojev on 07.07.2017.
 */
public class PropertyReader {

    private String filename;

    public PropertyReader() {
        this.filename = "config/db.config";
    }

    public PropertyReader(String filename) {
        this.filename = filename;
    }

    public String getProperty(String prop) {

        InputStream in = getClass().getClassLoader().getResourceAsStream(filename);
        Properties p = new Properties();
        try {
            p.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p.getProperty(prop);
    }
}
