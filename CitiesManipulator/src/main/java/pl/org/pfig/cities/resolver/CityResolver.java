package pl.org.pfig.cities.resolver;

import pl.org.pfig.cities.model.City;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablojev on 07.07.2017.
 */
public class CityResolver extends AbstractResolver<City> {

    private Connection connection;

    @Override
    public void connect(Connection connection)  {
        this.connection = connection;
    }

    @Override
    public List<City> get() {
        List<City> listOfCities = new ArrayList<>();
        try {
            String query = "SELECT * FROM `city`";

            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);


            while(rs.next()) {
                listOfCities.add(new City(rs.getInt("id"), rs.getString("city")));
            }

            rs.close();
            s.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return listOfCities;
    }

}
