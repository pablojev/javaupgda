package pl.org.pfig.cities.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pablojev on 07.07.2017.
 */
public class QueryHelper {
    public static String update(String tableName, Map<String, String> props) {
        String q = "UPDATE `" + tableName + "` SET ";
        String subquery = "";
        int id = Integer.valueOf(props.get("id"));
        props.remove("id");

        for (Map.Entry<String, String> e : props.entrySet()) {
            subquery += " `" + e.getKey() + "` = '" + e.getValue() + "',";
        }
        q += subquery.substring(0, subquery.length() - 1);
        q += " WHERE `id` = " + id;
        return q;
    }

    public static String delete(String tableName, int id) {
        return "DELETE FROM `" + tableName + "` WHERE `id` = " + id;
    }

    public static String insert(String tableName, Map<String, String> props) {
        // INSERT INTO `tableName` VALUES (values);
        // INSERT INTO `student` (name, lastname) VALUES ( 'pawel', 'asdasd', );

        if(props.containsKey("id")) {
            props.remove("id");
        }

        String ret = "INSERT INTO `" + tableName +"` ";
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();

        for(Map.Entry<String, String> e : props.entrySet()) {
            keys.add(e.getKey());
            values.add(e.getValue());
        }

        ret += generateBracketsValues(keys, "`");
        ret += " VALUES " + generateBracketsValues(values, "'");
        return ret;
    }
//+select(tableName:String, props:Map<String, String>):String


    private static String generateBracketsValues(List<String> args, String escapeCharacter) {
        String ret = "( ";
        for (String str : args) {
            if (escapeCharacter.length() > 0) ret += escapeCharacter;
            ret += str;
            if (escapeCharacter.length() > 0) ret += escapeCharacter;
            ret += ", ";
        }
        if (args.size() > 0) ret = ret.substring(0, ret.length() - 2) + " ";
        ret += " )";
        return ret;
    }


}






