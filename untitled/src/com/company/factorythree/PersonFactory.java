package com.company.factorythree;

import java.util.Random;

/**
 * Created by pablojev on 22.06.2017.
 */
public class PersonFactory {
    public static Person getPerson(String name) {
        return new Person(name, "Testowy", new Random().nextInt(50) + 18);
    }
}
