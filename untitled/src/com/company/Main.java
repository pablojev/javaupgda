package com.company;

import com.company.factory.AnimalFactory;
import com.company.factory.AnimalInterface;
import com.company.factoryother.Coupon;
import com.company.factoryother.CouponFactory;
import com.company.factorythree.Person;
import com.company.factorythree.PersonFactory;
import com.company.fluidinterface.Beer;
import com.company.fluidinterface.People;
import com.company.singleton.SingletonExample;
import com.company.templatemethod.Laptop;
import com.company.templatemethod.MidiTowerComputer;
import com.company.templatemethod.PersonalComputer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        /* Singleton Pattern */
        SingletonExample se1 = SingletonExample.getInstance();
        se1.setName("pawel");
        SingletonExample se2 = SingletonExample.getInstance();
        System.out.println(se2.getName());

        /* Template Method Pattern */
        Laptop laptop = new Laptop();
        System.out.println("Laptop:");
        laptop.devices();

        MidiTowerComputer mtc = new MidiTowerComputer();
        System.out.println("MidiTowerComputer:");
        mtc.devices();

        PersonalComputer pc = new PersonalComputer();
        System.out.println("PersonalComputer:");
        pc.devices();

        /* Factory Pattern */
        AnimalInterface cat = AnimalFactory.getAnimal("cat");
        cat.getSound();

        AnimalInterface dog = AnimalFactory.getAnimal("dog");
        dog.getSound();

        AnimalInterface frog = AnimalFactory.getAnimal("frog");
        frog.getSound();

        //AnimalInterface tiger = AnimalFactory.getAnimal("tiger");

        /* Factory Pattern 2 */
        int[] numbers = new int[6];
        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = new Random().nextInt(48) + 1;
        }

        Coupon c = CouponFactory.getCoupon(numbers);

        System.out.println(c.getA());

        /* Factory Example 3 */

        Person pawel = PersonFactory.getPerson("Pawel");

        System.out.println(pawel);

        /* Chaining */

        Beer piwo = new Beer();

        piwo
                .setName("Specjal")
                .setType("Lager")
                .setTaste("Bitter")
                .setPrice(2.19);

        System.out.println(piwo);

        /* Fluid Interface */

        List<Person> people = new LinkedList<>();

        people.add(new Person("Pawel", "Testowy", 30));
        people.add(new Person("Pawel", "Inne", 42));
        people.add(new Person("Mirek", "Przykladowy", 22));
        people.add(new Person("Darek", "Testowy", 44));

        People pp = new People().addGroup("staff", people);

        for(Person pers : pp.from("staff").lastname("Testowy").name("Darek").get()) {
            System.out.println(pers);
        }


    }
}
