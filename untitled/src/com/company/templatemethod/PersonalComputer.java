package com.company.templatemethod;

/**
 * Created by pablojev on 22.06.2017.
 */
public class PersonalComputer extends BasicComputer {
    @Override
    public void externalDevice() {
        System.out.println("mouse & keyboard");
    }
}
