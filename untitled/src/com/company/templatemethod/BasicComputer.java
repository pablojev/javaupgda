package com.company.templatemethod;

/**
 * Created by pablojev on 22.06.2017.
 */
public abstract class BasicComputer {

    public void devices() {
        motherboard();
        processor();
        externalDevice();
        System.out.println();
    }

    public void motherboard() {
        System.out.println("Motherboard");
    }

    public void processor() {
        System.out.println("Processor");
    }

    public abstract void externalDevice();
}
