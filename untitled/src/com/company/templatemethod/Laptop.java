package com.company.templatemethod;

/**
 * Created by pablojev on 22.06.2017.
 */
public class Laptop extends BasicComputer {
    @Override
    public void externalDevice() {
        System.out.println("Pendrive");
    }
}
