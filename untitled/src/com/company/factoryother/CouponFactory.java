package com.company.factoryother;

/**
 * Created by pablojev on 22.06.2017.
 */
public class CouponFactory {

    private CouponFactory() { }

    public static Coupon getCoupon(int[] numbers) {
        return new Coupon(  numbers[0],
                            numbers[1],
                            numbers[2],
                            numbers[3],
                            numbers[4],
                            numbers[5]
                );
    }
}
