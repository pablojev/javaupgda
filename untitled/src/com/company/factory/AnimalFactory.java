package com.company.factory;

/**
 * Created by pablojev on 22.06.2017.
 */
public class AnimalFactory {

    public static AnimalInterface getAnimal(String animal) {
        animal = animal.toLowerCase();
        switch(animal) {
            case "dog":
                return new Dog();
            case "frog":
                return new Frog();
            case "cat":
                return new Cat();
        }
        throw new IllegalArgumentException("Unknown animal.");
    }
}
