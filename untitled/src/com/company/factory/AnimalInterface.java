package com.company.factory;

/**
 * Created by pablojev on 22.06.2017.
 */
public interface AnimalInterface {
    public void getSound();
}
