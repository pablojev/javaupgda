package com.company.factory;

/**
 * Created by pablojev on 22.06.2017.
 */
public class Cat implements AnimalInterface {
    @Override
    public void getSound() {
        System.out.println("miau miau");
    }
}
