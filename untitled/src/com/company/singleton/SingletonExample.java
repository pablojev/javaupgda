package com.company.singleton;

public class SingletonExample {

    private static SingletonExample _instance;
    private String name;

    private SingletonExample() {}

    public static SingletonExample getInstance() {
        // lazy initialization
        if(_instance == null) {
            System.out.println("Tworze instancje.");
            _instance = new SingletonExample();
        }
        System.out.println("Zwracam instancje, bo juz jest utworzona.");
        return _instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
