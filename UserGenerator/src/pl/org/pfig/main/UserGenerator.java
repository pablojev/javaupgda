package pl.org.pfig.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	private final String path = "resources/";

	public User getRandomUser() {
		UserSex us = UserSex.SEX_MALE;
		if(new Random().nextInt(2) == 0) {
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us);
	}
	
	public User getRandomUser(UserSex sex) {
		User currentUser = new User();
		currentUser.setSex(sex);
		String name = "";
		if(sex.equals(UserSex.SEX_MALE)) {
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		currentUser.setAddress(getRandomAddress());
		String dt = getRandomBirthDate(); // dd.MM.yyyy
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		return currentUser;
	}
	
	
	private Address getRandomAddress() {
		Address adr = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
		adr.setCountry("Poland");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setStreet("ul. " + getRandomLineFromFile("street.txt"));
		adr.setNumber("" + (new Random().nextInt(100) + 1));
		return adr;
	}
	
	private int countLines(String filename) {
		int lines = 1;
		try(Scanner sc = new Scanner(new File(path + filename))) {
			while(sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return lines;
	}
	
	private String getRandomLineFromFile(String filename) {
		int allLines = countLines(filename);
		int line = new Random().nextInt(allLines) + 1;
		int currLine = 1;
		String currentLineContent = "";
		try(Scanner sc = new Scanner(new File(path + filename))) {
			while(sc.hasNextLine()) {
				currentLineContent = sc.nextLine();
				if(line == currLine) {
					return currentLineContent;
				}
				currLine++;
			}
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}
	
	private String getRandomBirthDate() {
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		// a. jest podzielny przez 4, ale nie jest podzielny przez 100
		// b. jest podzielny przez 400
		if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
			 daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return leadZero(day) + "." + leadZero(month) + "." + year; // dd.MM.YYYY;
	}
	
	private String leadZero(int arg) {
		if(arg < 10) return "0" + arg;
		else return "" + arg;
	}
	
}
