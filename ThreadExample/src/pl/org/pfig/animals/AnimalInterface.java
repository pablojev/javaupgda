package pl.org.pfig.animals;

@FunctionalInterface 
public interface AnimalInterface {
	public String[] make(String anims);
}
