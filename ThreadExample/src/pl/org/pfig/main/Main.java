package pl.org.pfig.main;

import java.util.LinkedList;
import java.util.List;

import pl.org.pfig.animals.Animal;
import pl.org.pfig.calc.Calc;
import pl.org.pfig.calc.CalculatorInterface;
import pl.org.pfig.employee.Company;
import pl.org.pfig.employee.Employee;

public class Main {
	public static void main(String[] args) {
		
		List<Employee> listOfEmps = new LinkedList<>();
		
		listOfEmps.add(new Employee("Paweł", "Testowy", 18, 3000));
		listOfEmps.add(new Employee("Jola", "Przykladowy", 19, 5000));
		listOfEmps.add(new Employee("Piotr", "Przykladowy", 19, 5000));
		
		Company cc = new Company();
		cc.setEmployees(listOfEmps);
		
		cc.filter( n -> n.startsWith("J"), t -> t.toUpperCase());
		
		System.exit(0);
		String s = "test";
		
		System.out.println(s.startsWith("te"));
		
		
		Thread t = new Thread(new PrzykladowyWatek());
		
		t.start();
		
		new Thread(new InnyWatek(), "Jakis Watek").start();
		System.out.println("KONIEC");
		
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Pochodze z kodu Runnable");
			}
		}).start();	
		
		
		new Thread(  () -> {
			int a = 1, b =3;
			System.out.println("Suma wynosi: " + (a + b));
		}  ).start();
		
		
		new Thread(  () -> System.out.println("Wiadomosc z FI")   ).start();		
		
		
		
		List<Person> p = new LinkedList<>();
		
		p.add(new Person(1));
		
		
		Calc c = new Calc();
		
		//System.out.println(c.add(3, 4));
		
		System.out.println( c.oper(5, 6, new CalculatorInterface() {
			
			@Override
			public double doOperation(int a, int b) {
				return a + b;
			}
		}) );
		
		System.out.println( c.oper(17,  3, ( liczba1, liczba2) -> ( liczba1 - liczba2 ) ) );
		
		System.out.println( c.oper(17,  3, (a, b) -> ( a * b ) ) );
		
		
		Animal a = new Animal();
		a.addAnimal("slon", (anims) -> (anims.split(" ")));
		
		a.addAnimal("pies|kot|zaba", (anims) -> (anims.split("\\|")));
		
		a.addAnimal("aligator_waz", (anims) -> (anims.split("_")));
		
		a.addAnimal("katp", (anims) -> {
			String[] ret = new String[1];
			ret[0] = "";
			for(int i = anims.length() - 1; i >= 0; i--) {
				ret[0] += anims.charAt(i) + "";
			} 
			return ret;
		});
		
		//System.out.println(a.containsAnimal("katp"));

		//System.out.println(a.containsAnimal("ptak|kot", (s) -> s.split("\\|")));
		
		
	}
}
