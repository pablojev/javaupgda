package pl.org.pfig.employee;

@FunctionalInterface
public interface TransformInterface {
	public String transform(String s);
}
