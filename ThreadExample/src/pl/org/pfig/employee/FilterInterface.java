package pl.org.pfig.employee;

@FunctionalInterface
public interface FilterInterface {
	public boolean test(String s);
}
