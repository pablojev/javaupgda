package decorator.person;

/**
 * Created by Adrian on 2017-06-23.
 */
public class ConsoleTest {

    public static void main(String[] args) {


        PersonPrinter personPrinter = new EyesPrinter(new AgePrinter(new BasicDataPrinter()));
        Person person = new Person("Jan", "Kowalski", 20, 81.5, 175, Person.EyesColor.BLUE);
        FilePrinter filePrinter = new FilePrinter(personPrinter);
        filePrinter.printToFile(person, "person.txt");
    }

}
