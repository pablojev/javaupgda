package decorator.person;

/**
 * Created by Adrian on 2017-06-23.
 */
public class ConsolePrinter {
    private final PersonPrinter personPrinter;

    public ConsolePrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void print(Person person) {
        personPrinter.print(person, System.out);
    }
}
