package decorator.person;

import java.io.PrintStream;

/**
 * Created by Adrian on 2017-06-23.
 */
public interface PersonPrinter {

    void print(Person person, PrintStream out);
}
