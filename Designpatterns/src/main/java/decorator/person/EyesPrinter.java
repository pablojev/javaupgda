package decorator.person;

import java.io.PrintStream;

/**
 * Created by Adrian on 2017-06-23.
 */
public class EyesPrinter implements PersonPrinter {

    private final PersonPrinter personPrinter;

    public EyesPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    @Override
    public void print(Person person, PrintStream out) {
        personPrinter.print(person, out);
        out.println("eyes-color : " + person.getEyesColor());
    }
}
