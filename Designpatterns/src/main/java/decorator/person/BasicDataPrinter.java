package decorator.person;

import java.io.PrintStream;

/**
 * Created by Adrian on 2017-06-23.
 */
public class BasicDataPrinter implements PersonPrinter {
    @Override
    public void print(Person person, PrintStream out) {
        out.println("name : " + person.getName());
        out.println("surname : " + person.getSurname());
    }
}
