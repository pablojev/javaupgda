package queue.generic;

import java.util.List;

/**
 * Created by Adrian on 2017-06-27.
 */
public class Main {

    public static void main(String[] args) {

        SimpleQueue<String> queue = new SimpleArrayQueue<>();
        queue.offer("AA");
        queue.offer("BB");
        queue.offer("CC");

        System.out.println(queue.peek());

        while (!queue.isEmpty()) {
            System.out.println(queue.poll());
        }

    }
}
