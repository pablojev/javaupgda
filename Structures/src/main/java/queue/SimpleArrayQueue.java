package queue;

import java.util.NoSuchElementException;

/**
 * Created by Adrian on 2017-06-27.
 */
public class SimpleArrayQueue implements SimpleQueue {
    private int[] data;
    private int start = 0;
    private int end = 0;
    private boolean isEmpty = true;

    public SimpleArrayQueue() {
        this(8);
    }

    public SimpleArrayQueue(int size) {
        data = new int[size];
    }

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(int value) {
        if (isFull()) {
            createNewArray();
        }

        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    private void createNewArray() {
        int[] newData = new int[data.length*2];
        int i = 0;
        while (!isEmpty) {
            newData[i] = poll();
            i++;
        }

        data = newData;
        start = 0;
        end = i;
    }

    private boolean isFull() {
        return start == end && !isEmpty;
    }

    @Override
    public int poll() {
        if (!isEmpty) {
            int result = data[start];
            start = (start + 1) % data.length;
            if (start == end) {
                isEmpty = true;
            }
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public int peek() {
        if (!isEmpty) {
            return data[start];
        }else {
            throw new NoSuchElementException();
        }
    }
}
