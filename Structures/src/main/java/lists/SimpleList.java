package lists;

public interface SimpleList {
    void add(int value);
    int get(int index);
    void add(int value, int index);
    boolean contain(int value);
    void remove(int index);
    void removeValue(int value);
    int size();
}
