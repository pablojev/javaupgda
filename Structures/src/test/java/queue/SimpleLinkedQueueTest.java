package queue;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Adrian on 2017-06-27.
 */
public class SimpleLinkedQueueTest {

    SimpleQueue underTest;

    @Test
    public void offerAnPoll3Elements() {
        underTest = new SimpleLinkedQueue();
        underTest.offer(2);
        underTest.offer(4);
        underTest.offer(6);

        assertThat(underTest.isEmpty()).isFalse();
        assertThat(underTest.poll()).isEqualTo(2);
        assertThat(underTest.poll()).isEqualTo(4);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerPeekAndPool2Times() {
        underTest = new SimpleLinkedQueue();
        underTest.offer(2);
        underTest.peek();
        underTest.poll();
        underTest.offer(6);

        assertThat(underTest.peek()).isEqualTo(6);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll100Elements() {
        underTest = new SimpleLinkedQueue();
        for (int i = 0; i < 100; i++) {
            underTest.offer(i);
        }

        for (int i = 0; i < 100; i++) {
            assertThat(underTest.poll()).isEqualTo(i);
        }
    }

    @Test(expected = NoSuchElementException.class)
    public void pollFromEmptyQueue() {
        underTest = new SimpleLinkedQueue();

        underTest.poll();
    }

    @Test(expected = NoSuchElementException.class)
    public void peekFromEmptyQueue() {
        underTest = new SimpleLinkedQueue();

        underTest.peek();
    }
}