function fillTemplateByObject(template, obj) {
    if(!(obj instanceof Array)) {
        obj = [ obj ];
    }
    var ret = "";
    for(let i = 0; i < obj.length; i++) {
        var keys = Object.keys(obj[i]);
        var newTemp = "" + template;
        for(let key of keys) {
            newTemp = newTemp.replace('[% ' + key + ' %]', obj[i][key]);
        }
        ret += newTemp;
    }
    return ret;
}

var data = [
    {
        title: "Drugi post",
        body: "Przykładowy drugi post",
        date: "20.07.2017"
    },
    {
        title: "Trzeci post",
        body: "Przykładowy drugi post",
        date: "20.07.2017"
    },
    {
        title: "Czwarty post",
        body: "Przykładowy drugi post",
        date: "20.07.2017"
    }

];

var template = '<div class="postContainer">'
    + '<h2>[% title %]</h2>'
    + '<p>[% body %]</p>'
    + '</div>';

var body = document.querySelector('.container');

body.innerHTML = fillTemplateByObject(template, data);







