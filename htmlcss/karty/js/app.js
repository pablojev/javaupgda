$(document).ready(function() {
    $('.card').addClass('mt-30');
    $('.card').on('click', function(e) {
        if($(this).hasClass('mt-30')) {
            $(this).removeClass('mt-30');
        } else {
            $(this).addClass('mt-30');
        }
    });
});