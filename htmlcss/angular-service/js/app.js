var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: './views/main.html',
            controller: 'mainController'
        })
        .when('/names', {
            templateUrl: './views/names.html',
            controller: 'namesController'
        });
});

app.service('namesService', function() {
    this.names = [];
    this.msg = undefined;
    this.add = function(name, msg) {
        this.names.push(name);
        this.msg = msg;
    }
    this.get = function() {
        return this.names;
    }
    this.getMessage = function() {
        var msg = this.msg;
        this.msg = undefined;
        return msg;
    }
});

app.controller('mainController', function($scope, $window, namesService) {
    $scope.add = function() {
        namesService.add($scope.name, "Dodano imię!");
        $window.location.href = "/#!names";
    }
});

app.controller('namesController', function($scope, $timeout, namesService) {
    $scope.names = namesService.get();
    $scope.message = namesService.getMessage();
    $timeout(function() {
        $scope.message = undefined;
    }, 3000);
});