import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Adrian on 2017-06-26.
 */
public class MyMathTest {

    @Test
    public void sum() throws Exception {
        assert MyMath.sum(1,3) == 4;
    }

    @Test
    public void pow() throws Exception {
        assert MyMath.pow(2,3) == 8;
    }

    @Test
    public void find() throws Exception {
        int[] data = {1,2,6,9,11,13,23,41,43,54,90};

        assert MyMath.find(data, 13) == 5;
    }

    @Test
    public void findBisectionInArrayOfOddSize() throws Exception {
        int[] data = {1,2,6,9,11,13,23,41,43,54,90};

        assert MyMath.findBisection(data, 13) == 5;
        assert MyMath.findBisection(data, 5) == -1;
        assert MyMath.findBisection(data, 23) == 6;
    }

    @Test
    public void findBisectionInArrayOfEvenSize() throws Exception {
        int[] data = {1,2,6,9,11,13,23,41,43,54,90,100};

        assert MyMath.findBisection(data, 13) == 5;
        assert MyMath.findBisection(data, 5) == -1;
        assert MyMath.findBisection(data, 23) == 6;
    }

}