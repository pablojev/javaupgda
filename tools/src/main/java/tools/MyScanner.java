package tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MyScanner {
	
	private BufferedReader reader;
	
	public MyScanner() {
		this(System.in);
	}
	
	public MyScanner(InputStream in) {
		reader = new BufferedReader(new InputStreamReader(in));
	}
	
	public int nextInt() {
		try {
			return Integer.parseInt(reader.readLine());
		} catch (NumberFormatException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public int[] nextIntArray() {
		
		try {
			String[] numbers = reader.readLine().replace("]", "").replace("[", "").split(",");
			int[] results = new int[numbers.length];
			for(int i = 0 ; i < numbers.length ; i++) {
				results[i] = Integer.parseInt(numbers[i]);
			}
			return results;
		} catch (NumberFormatException | IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
}
