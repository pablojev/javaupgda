/**
 * Created by Adrian on 2017-06-27.
 */
public class GenericSort {

    public static <T extends Comparable<T>> void bubbleSort(T[] tab) {

        for (int i = 0; i < tab.length; i++) {
            for (int j = tab.length - 1; j > i; j--) {
                T left = tab[j - 1];
                T right = tab[j];

                if (left.compareTo(right) > 0) {
                    tab[j - 1] = right;
                    tab[j] = left;
                }
            }
        }
    }

    public static void selectionSort(int[] data) {
        for (int i = 0; i < data.length; i++) {

            int min = data[i];
            int minIndex = i;
            for (int j = i + 1; j < data.length; j++) {
                if (data[j] < min) {
                    min = data[j];
                    minIndex = j;
                }
            }

            int buffer = data[i];
            data[i] = min;
            data[minIndex] = buffer;


        }
    }

    public static void insertSort(int[] data) {
        for (int i = 0; i < data.length; i++) {
            int currentElement = data[i];
            int position = 0;
            while (data[position] < currentElement && position <= i) {
                position++;
            }

            for (int j = i; j > position; j--) {
                data[j] = data[j - 1];
            }

            data[position] = currentElement;
        }
    }

    public static void sortZegarski(int[] tab) {
        int temp;
        boolean isTabSorted = false;

        do {
            isTabSorted = true;
            for (int i = 0; i < tab.length - 1; i++) {
                if (tab[i] > tab[i + 1]) {
                    temp = tab[i];
                    tab[i] = tab[i + 1];
                    tab[i + 1] = temp;
                    isTabSorted = false;
                }
            }
        } while (!isTabSorted);
    }

    public static void sortBodzio(int[] data) {
        int size = data.length;
        int count = 1;
        while (count > 0) {
            count = 0;
            size--;
            for (int i = 0; i < size; i++) {
                int left = data[i];
                int right = data[i + 1];
                if (left > right) {
                    data[i] = right;
                    data[i + 1] = left;
                    count++;
                }
            }
        }
    }

    public static void sortGrrreg(int[] tab) {
        int helper = 0;

        for (int i = 0; i < tab.length - 1; i++) {
            if (tab[i] > tab[i + 1]) {
                helper = tab[i + 1];
                tab[i + 1] = tab[i];
                tab[i] = helper;
                sortGrrreg(tab);
                break;
            }
        }
    }

    public static void mergeSort(int[] data) {
        if (data.length > 1) {
            int middle = data.length / 2;
            int[] left = copy(0, middle-1, data);
            int[] right = copy(middle, data.length-1, data);
            mergeSort(left);
            mergeSort(right);
            merge(left,right,data);
        }
    }

    public static void merge(int[] left, int[] right, int[] data) {
        int a = 0;
        int b = 0;
        int i = 0;

        while (a < left.length && b < right.length) {
            if (left[a] <= right[b]) {
                data[i] = left[a];
                a++;
            }else {
                data[i] = right[b];
                b++;
            }
            i++;
        }

        while (a < left.length) {
            data[i++] = left[a++];
        }

        while (b < right.length) {
            data[i++] = right[b++];
        }
    }

    public static int[] copy(int start, int end, int[] data) {
        int j = 0;
        int[] copy = new int[end - start + 1];
        for (int i = start ; i <= end ; i++) {
            copy[j++] = data[i];
        }
        return copy;
    }
}

