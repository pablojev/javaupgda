/**
 * Created by Adrian on 2017-06-28.
 */
public class Main {

    public static void main(String[] args) {
        String[] names = {"ca", "cc", "aa", "ab"};
        GenericSort.bubbleSort(names);
        for (String name : names) {
            System.out.println(name);
        }

    }
}
