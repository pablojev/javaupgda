package pl.pawel.loginsystem.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pablojev on 04.08.2017.
 */
@RestController
@CrossOrigin
public class ExampleController {

    @RequestMapping("/")
    public String home() {
        return "go /show";
    }

    @RequestMapping("/show")
    @Secured("ROLE_ADMIN")
    public String show() {
        return "jestes zalogowany!";
    }
}
