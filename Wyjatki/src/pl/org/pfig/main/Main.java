package pl.org.pfig.main;

import java.util.*;

public class Main {

	public static void main(String[] args)  {
		ExceptionSimpleClass esc = new ExceptionSimpleClass();
//		
		try {
			esc.exceptionExample("pawel");	
		} catch(PawelException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			esc.make(55);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
			//System.out.println("[ ERROR ] " + e.getMessage());
		} catch(Exception e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("[ END ]");
		}
	
		int[] array = new int[10];
		
		// wczytajmy z klawiatury od użytkownika 10 wartości do tablicy
		Scanner sc = new Scanner(System.in);
		
		for(int i = 0; i < 10; i++) {
			System.out.println("Podaj array[" + i + "] = ");
			array[i] = sc.nextInt();
		}
		
		try {
			System.out.println(array[10]);
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Brak elementu pod indeksem: " + e.getMessage());
		}
	}

}
