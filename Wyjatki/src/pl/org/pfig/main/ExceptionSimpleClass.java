package pl.org.pfig.main;

public class ExceptionSimpleClass {
	public void make(int a) throws IllegalArgumentException, Exception {
		if(a == 55) {
			throw new IllegalArgumentException("Niepoprawny argument");
		}
		
		if(a == 5) {
			throw new Exception("a jest równe 5!");
		}
	
		
		System.out.println("a jest poprawną wartością");
		
	}
	
	public void exceptionExample(String name) throws PawelException {
		if(name.equalsIgnoreCase("pawel")) {
			throw new PawelException("Jestes pawlem!");
		}
	}
}
