package pl.pawel.moviedb.servlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.pawel.moviedb.entity.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by pablojev on 24.07.2017.
 */
@WebServlet(name = "MovieServlet")
public class MovieServlet extends HttpServlet {
    SessionFactory sf = new Configuration().configure().buildSessionFactory();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action.equals("add")) {
            String name = request.getParameter("movieName");
            Session session = sf.openSession();
            Movie m = new Movie(name);
            Transaction t = session.beginTransaction();
            session.save(m);
            t.commit();
            session.close();
        } else if(action.equals("edit")) {
            String name = request.getParameter("movieName");
            int id = Integer.valueOf(request.getParameter("id"));
            Movie m = new Movie(name);
            m.setId(id);
            Session session = sf.openSession();
            Transaction t = session.beginTransaction();
            session.update(m);
            t.commit();
            session.close();
        }

        //request.getRequestDispatcher("/MovieServlet?action=show").forward(request, response);
        response.sendRedirect("MovieServlet?action=show");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session session = sf.openSession();
        String action = request.getParameter("action");

        if(action.equals("show")) {
            Transaction t = session.beginTransaction();
            List<Movie> movies = session.createQuery("from Movie where id > 0").list();
            t.commit();
            request.setAttribute("movies", movies);
            request.getRequestDispatcher("movies.jsp").forward(request, response);
        } else if(action.equals("edit")) {
            int id = Integer.valueOf(request.getParameter("id"));
            Movie m = session.get(Movie.class, id);
            request.setAttribute("movie", m);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
        } else if(action.equals("delete")) {
            int id = Integer.valueOf(request.getParameter("id"));
            Movie m = new Movie();
            m.setId(id);
            Transaction t = session.beginTransaction();
            session.delete(m);
            t.commit();
            request.getRequestDispatcher("MovieServlet?action=show").forward(request, response);
        }


        session.close();
    }
}
