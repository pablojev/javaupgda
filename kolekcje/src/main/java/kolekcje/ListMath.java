package kolekcje;

import java.util.List;

public class ListMath {

	
	public static int sum(List<Integer> numbers) {
		int sum = 0;
		for(int i : numbers) {
			sum += i;
		}
		
		return sum;
	}
	
	public static int sumFor(List<Integer> numbers) {
		int sum = 0;
		for(int i = 0 ; i < numbers.size() ; i++) {
			sum += numbers.get(i);
		}
		
		return sum;
	}
	
	public static double mean(List<Integer> numbers) {
		
		return (double) sum(numbers) / numbers.size();
	}
}
