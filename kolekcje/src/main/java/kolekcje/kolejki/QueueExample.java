package kolekcje.kolejki;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExample {

	public static void main(String[] args) {
		
		
		Queue<Integer> queue = new PriorityQueue<>(); // LinkedList
		queue.offer(1);
		queue.offer(21);
		queue.offer(2);
		queue.offer(10);
		queue.offer(12);
		
		while(!queue.isEmpty()) {
			System.out.println(queue.poll());
		}
		System.out.println(queue.poll());
	}

}
