package kolekcje.kolejki;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		persons.offer(new Person("Jan", 16));
		persons.offer(new Person("Anna", 15));
		persons.offer(new Person("Janina", 30));
		persons.offer(new Person("Eustachy", 23));
		
		while (!persons.isEmpty()) {

			System.out.println(persons.poll());
		}
	}

}
