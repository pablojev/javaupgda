package kolekcje;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {

	public static boolean containDuplicates(String text) {

		char[] chtr = text.toCharArray();

		Set<Character> set = new HashSet<>();
		for (char i : chtr) {
			set.add(i);
		}
		
		if (set.size() != chtr.length) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean containDuplicates1(String text) {

		Set<Character> set = new HashSet<>();
		for (char i : text.toCharArray()) {
			set.add(i);
		}
		return set.size() != text.length();
	}
}
