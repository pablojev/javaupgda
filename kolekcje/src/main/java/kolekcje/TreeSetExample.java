package kolekcje;

import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetExample {
	
	
	public static void main(String[] args) {
		TreeSet<Integer> treeSet = new TreeSet<>(Arrays.asList(1,2,3,4,5,6));
		System.out.println("Ca�y zbi�r");
		for (int integer : treeSet) {
			System.out.println(integer);
		}
		System.out.println("Ca�y headSet(3)");
		for (Integer integer : treeSet.headSet(3)) {
			System.out.println(integer);
		}

		System.out.println("Ca�y tailSet(3)");
		for (Integer integer : treeSet.tailSet(3)) {
			System.out.println(integer);
		}
	}

}
