package kolekcje;

import java.util.Arrays;
import java.util.List;

public class ListAndArray {

	public static void main(String[] args) {
		int[] array = {4,2,2,1,5,29,3,8};
		List<Integer> list = Arrays.asList(1,2,4,2,5,12);
		
		System.out.println(countDuplicates(array, list));
	}
	
	public static int countDuplicates(int[] array, List<Integer> list) {
		int count = 0;

		if (array.length != list.size()) {
			throw new RuntimeException("Kolekcje s� r�nej d�ugo�ci array : " + array.length + "list : "+ list.size());
		}
		
		for(int i =0 ; i < array.length; i++) {
			if (array[i] == list.get(i)) {
				count++;
			}
		}
		
		
		return count;
	}
}
