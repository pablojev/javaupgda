package kolekcje.mapy;

public class UnivesityExample {

	public static void main(String[] args) {

		University uni = new University();
		uni.addStudent(100200, "Daniel", "Bogdalski");
		uni.addStudent(100400, "Jacek", "Kowalski");
		uni.addStudent(200200, "Anna", "Nowak");

		System.out.println(uni.studentExists(100200));

		System.out.println(uni.getStudent(100400).getName());

		System.out.println(uni.studentsNumber());

		uni.showAll();
	}

}
