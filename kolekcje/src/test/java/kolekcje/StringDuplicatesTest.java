package kolekcje;

import org.junit.Test;

public class StringDuplicatesTest {

	@Test
	public void containDuplicatesTest() {
		

		assert StringDuplicates.containDuplicates("abcd") == false;
		assert StringDuplicates.containDuplicates("abad") == true;
	}
}
