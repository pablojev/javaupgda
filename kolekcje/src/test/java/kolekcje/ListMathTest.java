package kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ListMathTest {

	
	@Test
	public void sumTest() {
		assert ListMath.sum(generateData()) == 15;
		
	}
	
	@Test
	public void sumTestFor() {
		assert ListMath.sumFor(generateData()) == 15;
		
	}

	@Test
	public void meanTestFor() {
		assert ListMath.mean(generateData()) == 5.0;
		
	}
	private List<Integer> generateData() {
		
		return new ArrayList<Integer>(Arrays.asList(4,5,6));
	}
}
