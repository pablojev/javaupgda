package pl.org.pfig.data;

public class Dog implements AnimalInterface {
	private String name;
	private int legs;

	public Dog(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	public String leadZero(int num) {
		if(num < 10) {
			return "0" + num;
		} else {
			return "" + num;
		}
	}
	
	
	public int getLegs() {
		return legs;
	}

	public void setLegs(int legs) {
		if(legs > 0 && legs < 5)
			this.legs = legs;
	}

	public Dog newDog(String name) {
		return new Dog(name);
	}

	@Override
	public String toString() {
		return "Dog:"+name;
	}
	
	
	
	
}
