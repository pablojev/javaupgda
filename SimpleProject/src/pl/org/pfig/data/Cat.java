package pl.org.pfig.data;

public class Cat implements AnimalInterface, Soundable {
	private String name;
	
	public Cat(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public String getSound() {
		return "miau miau";
	}
}
