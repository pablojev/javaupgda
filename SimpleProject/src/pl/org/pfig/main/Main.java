package pl.org.pfig.main;

import pl.org.pfig.data.*;

public class Main {
	public static void main(String[] args) {
		Dog d1 = new Dog("Burek");
		Cat c1 = new Cat("Mruczek");
		Llama l1 = new Llama("SuperLama");
		
		System.out.println("Piesek to: " + d1.getName());
		System.out.println("Kotek to: " + c1.getName());
		System.out.println("Lama to: " + l1.getName());
		
		AnimalInterface[] animals = new AnimalInterface[3];
		animals[0] = d1;
		animals[1] = c1;
		animals[2] = l1;
		for(AnimalInterface ai : animals) {
			System.out.println("Zwierze nazywa si�: " + ai.getName() + " ("+ai.getLegs()+")");
			if(ai instanceof Soundable) {
				Soundable sound = (Soundable)ai;
				System.out.println("\trobi: " + sound.getSound());
			}
		}
		
		System.out.println(d1);
	}
}
