/**
 * Created by Adrian on 2017-07-04.
 */
public class InsertGenerate {

    public static void main(String[] args) {

        StringBuilder insert = new StringBuilder();
        insert.append("INSERT INTO `contant` VALUES ");
        ContactGenerator generator = new ContactGenerator();
        for(int i = 0 ; i < 1000 ; i++) {
            Contact contact = generator.generate();

            String line = String.format("(id,\"%s\",\"%s\",\"%s\",\"%s\")",
                    contact.getFirstName(),
                    contact.getLastName(),
                    contact.getNumber(),
                    contact.geteMail()
            );

            insert.append(line);
            if (i < 999) {
                insert.append(",\n");
            }
            else {
                insert.append(";\n");
            }
        }
        System.out.println(insert.toString());
    }
}
