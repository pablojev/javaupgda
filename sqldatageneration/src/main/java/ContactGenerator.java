import java.util.Random;
import java.util.stream.Stream;

/**
 * Created by Adrian on 2017-07-04.
 */
public class ContactGenerator {

    private String[] names = {"Jan", "Adam", "Dominik", "Jakub"};
    private String[] surNames = {"Kowalski", "Nowak", "Malinowski", "Kwiatkowski"};
    private Random random = new Random();

    public Contact generate() {

        String name = names[random.nextInt(names.length)];
        String surName = surNames[random.nextInt(surNames.length)];

        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 9 ; i++) {
            builder.append(random.nextInt(10));
        }
        String number = builder.toString();

        String eMail = name.toLowerCase() + "." +surName.toLowerCase() + "@gmail.com";

        return new Contact(name, surName, number, eMail);
    }

    public static void main(String[] args) {


        ContactGenerator generator = new ContactGenerator();
        for (int i = 0 ; i < 10 ; i++) {
            System.out.println(generator.generate());
        }

        Stream.generate(generator::generate).limit(10).forEach(System.out::println);
    }
}
