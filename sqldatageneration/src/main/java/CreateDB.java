import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by Adrian on 2017-07-04.
 */
public class CreateDB {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        //STEP 2: Register JDBC driver
       Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
        System.out.println("Connecting to database...");
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            System.out.println("Creating database...");

            try (Statement stmt = conn.createStatement()){
                String sql = "CREATE DATABASE NowaBaza";
                stmt.executeUpdate(sql);
                System.out.println("Database created successfully...");
            }
        }
        System.out.println("Goodbye!");
    }
}
