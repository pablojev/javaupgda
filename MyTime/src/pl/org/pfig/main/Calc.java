package pl.org.pfig.main;


/* przyklad na chaining */
public class Calc {
	private double num = 0;
	
	public Calc(int num) {
		this.num = num;
	}
	
	public Calc add(int arg) {
		this.num += arg; // this.num = this.num + arg;
		return this;
	}
	
	public Calc print() {
		System.out.println(this.num);
		return this;
	}
	
}
