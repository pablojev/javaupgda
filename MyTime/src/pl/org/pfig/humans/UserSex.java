package pl.org.pfig.humans;

public enum UserSex {
	SEX_MALE(1,"Mezczyzna"), 
	SEX_FEMALE(2, "Kobieta"), 
	SEX_OTHER(3, "Inny");
	
	private int id;
	private String commonName;
	
	private UserSex(int id, String sex) {
		this.commonName = sex;
		this.id = id;
	}

	public String getCommonName() {
		return commonName;
	}	
	
	public int getId() {
		return id;
	}
}
