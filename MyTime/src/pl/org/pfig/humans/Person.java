package pl.org.pfig.humans;

public class Person extends Humanoid {

	public Person() {
		
	}
	
	public Person(int limbs) {
		super(limbs);
	}
	
	public void printSquareLimbs() {
		super.setLimbs(5);
		int myLimbs = super.getLimbs() * super.getLimbs();
		System.out.println(super.name);
		System.out.println(myLimbs);
	}

}
