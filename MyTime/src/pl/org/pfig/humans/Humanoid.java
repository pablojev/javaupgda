package pl.org.pfig.humans;

public class Humanoid {
	private int limbs;
	public String name;
	
	public Humanoid() { }
	
	public Humanoid(int limbs) {
		this.limbs = limbs;
	}

	public int getLimbs() {
		return limbs;
	}

	public void setLimbs(int limbs) {
		this.limbs = limbs;
	}

	
	
}
