USE shop;

CREATE TABLE customer (
id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(30),
   last_name VARCHAR(30)
   );
   
CREATE TABLE `order` (
   id INT AUTO_INCREMENT PRIMARY KEY,
   date DATE,
   paid ENUM ('NEW', 'PAID', 'CANCELED', 'COMPLETION'),
   customer_id INT, 
   FOREIGN KEY (customer_id) REFERENCES customer(id)
   );
   
CREATE TABLE order_products (
   order_id INT, 
   product_id INT, 
   quantity INT UNSIGNED NOT NULL ,
   FOREIGN KEY (order_id) REFERENCES `order`(id),
   FOREIGN KEY (product_id) REFERENCES product(id),
   PRIMARY KEY (order_id,product_id)
   );
   
   
SELECT * FROM category LEFT JOIN product ON product.category_id = category.id;

INSERT INTO customer VALUES (id, "Jan", "Kowalski"), (id, "Adam", "Nowak");

INSERT INTO `order` VALUES (id, "2017-03-06", 'NEW', 1);
INSERT INTO `order` VALUES (id, "2017-03-07", 'NEW', 1);

SELECT * FROM product;

INSERT INTO order_products VALUES 
	(1,1,10),
	(1,2,1),
	(1,3,4),
	(1,4,6),
	(1,5,20),
	(2,2,3),
	(2,3,10),
	(2,4,40),
	(2,5,2),
	(2,6,3);

SELECT name, quantity FROM order_products LEFT JOIN product ON order_products.product_id = product.id WHERE order_products.order_id = 1;

CREATE VIEW order_full AS
SELECT SUM(op.quantity) AS products_quantity, o.`date`, concat(c.first_name, " ", c.last_name) AS customer_name, o.paid, SUM(p.price*op.quantity) AS amount
FROM order_products op 
LEFT JOIN
product p  
ON p.id = op.product_id
LEFT JOIN
`order` o
ON op.order_id = o.id
LEFT JOIN
customer c
ON c.id = o.customer_id
GROUP BY o.id;

SELECT * FROM order_full;


SELECT sum(amount) FROM order_full WHERE `date` BETWEEN "2017-02-01" AND "2017-02-31";
