

USE contacts;
DELIMITER //

CREATE PROCEDURE addContact() 
BEGIN
	INSERT INTO contact (first_name, last_name) VALUE ("Adam", "Nowak");
END;//
DELIMITER ;

CALL addContact();


SELECT count(*) FROM contact WHERE first_name = "Adam";


USE countrycapitol;
DROP PROCEDURE IF EXISTS NewState;
DELIMITER //
CREATE PROCEDURE NewState(IN state_name varchar(32)) 
BEGIN
	DECLARE id INT;
    
    INSERT INTO city (name,citizens) VALUE ("?", 100) ;
    SET id = LAST_INSERT_ID();

	INSERT INTO state (name, population, capital_id) VALUE (state_name, 1000, id);
END;//
DELIMITER ;



CALL NewState("Węgry");

SELECT * FROM state ;

USE shop;
DROP FUNCTION IF EXISTS ProductsQuantity;
DELIMITER //
CREATE FUNCTION ProductsQuantity(order_id int) RETURNS INT
BEGIN
	DECLARE c INT;
	SELECT sum(o.quantity) INTO c FROM order_products o WHERE o.order_id = order_id;
    RETURN c;
END;//
DELIMITER ;

SELECT ProductsQuantity(1);