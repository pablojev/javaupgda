CREATE DATABASE company;
DROP TABLE worker;

CREATE TABLE worker (
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(32),
salary DECIMAL(8,2),
age INT
);
SELECT * FROM worker;

DELETE FROM worker;
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1000, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1000, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1000, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1500, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1500, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1500, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 2000, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 2000, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 4000, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 4000, 25);

SELECT max(salary), min(salary), avg(salary) FROM worker;

SELECT count(*), salary FROM worker GROUP BY salary;
SELECT avg(salary), age FROM worker GROUP BY age;


SELECT * FROM worker ORDER BY salary;
SELECT * FROM worker ORDER BY salary DESC;


SELECT * FROM worker ORDER BY salary LIMIT 1;
SELECT * FROM worker ORDER BY salary DESC LIMIT 1;

SELECT * FROM worker WHERE salary = (SELECT max(salary) FROM worker);
SELECT abs(salary - (SELECT avg(salary) FROM worker)) as diff , name, id  FROM worker ORDER BY diff LIMIT 3;