SET SQL_SAFE_UPDATES = 0;


DROP DATABASE IF EXISTS `bank`;
CREATE DATABASE IF NOT EXISTS `bank`;
USE `bank`;

CREATE TABLE `account` (
`id` INT AUTO_INCREMENT PRIMARY KEY,
`number` CHAR(4),
`balance` DECIMAL(65,2)
);

INSERT INTO `account` (`number`, `balance`) VALUES ("1234", 1000), ("4321", 1000);

START TRANSACTION;
UPDATE `account` SET balance = balance - 100 WHERE number = "1234";
UPDATE `account` SET balance = balance + 100 WHERE number = "4321";
COMMIT;
ROLLBACK;

SELECT * FROM `account`;