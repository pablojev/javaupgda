DROP DATABASE IF EXISTS school;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE school;
USE school;

/*CREATE TABLE lesson(
`subject` VARCHAR(20),
teacher_name VARCHAR(20), 
teacher_surname VARCHAR(20),
start_time TIME,
end_time TIME
);*/

CREATE TABLE `subject`(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20)
);

CREATE TABLE teacher (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20), 
surname VARCHAR(20)
);

CREATE TABLE lesson_number (
id INT AUTO_INCREMENT PRIMARY KEY,
start_time TIME,
end_time TIME
);

CREATE TABLE lesson (
PRIMARY KEY (lesson_number_id,teacher_id),
subject_id INT,
lesson_number_id INT,
teacher_id INT,
FOREIGN KEY(subject_id) REFERENCES `subject`(id),
FOREIGN KEY(lesson_number_id) REFERENCES lesson_number(id),
FOREIGN KEY(teacher_id) REFERENCES teacher(id)
);

INSERT INTO `subject` (`name`) VALUES ("Matematyka"), ("J. Polski"), ("WF");
INSERT INTO teacher (`name`,`surname`) VALUES ("Jan", "Kowalski"), ("Barbara", "Nowak");
INSERT INTO lesson_number (start_time, end_time) VALUES ("8:00", "8:45"), ("8:50", "9:35");

INSERT INTO lesson VALUES (1,1,1), (1,2,1), (2,1,2), (2,2,2);

SELECT s.name AS `subject` , t.name AS teacher_name, t.surname teacher_surname, ln.start_time AS start_time, ln.end_time AS end_time
FROM lesson l 
LEFT JOIN subject s ON l.subject_id = s.id
LEFT JOIN teacher t ON l.teacher_id = t.id
LEFT JOIN lesson_number ln ON l.lesson_number_id = ln.id;

CREATE VIEW lesson_full AS
SELECT s.name AS `subject` , t.name AS teacher_name, t.surname teacher_surname, ln.start_time AS start_time, ln.end_time AS end_time
FROM lesson l 
LEFT JOIN subject s ON l.subject_id = s.id
LEFT JOIN teacher t ON l.teacher_id = t.id
LEFT JOIN lesson_number ln ON l.lesson_number_id = ln.id;

SELECT * FROM lesson_full;