DROP DATABASE IF EXISTS personal_data;


CREATE DATABASE personal_data;

CREATE TABLE person (
	first_name VARCHAR(32),
	last_name VARCHAR(32),
	age TINYINT UNSIGNED
);

/*
 

*/

INSERT INTO person VALUES 
("Jan", "Kowalski", 21),
("Adam", "Kowalski", 22),
("Dominik", "Kowalski", 23),
("Jan", "Nowak", 24),
("Adam", "Nowak", 25);

SELECT * FROM person;
SELECT first_name FROM person;

SELECT count(*), age FROM person GROUP BY age;
