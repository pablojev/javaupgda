DROP DATABASE countryCapitol;
CREATE DATABASE countryCapitol;
USE countryCapitol;

CREATE TABLE city (
    id INT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(30),
   citizens INT
   );

CREATE TABLE state (
    id INT AUTO_INCREMENT PRIMARY KEY,
   name varchar(30),
   population INT,
   capital_id INT, 
   FOREIGN KEY(capital_id) REFERENCES city(id)
   );
   
   
INSERT INTO city (name,citizens) VALUES
('Praga',1280500),
('Paryż',2000000),
('Wilno',539900),
('Berlin',3400000),
('Warszawa',1753977),
('Olsztyn',173500),
('Gdańsk',463700),
('Sopot',37650),
('Gdynia',247500),
('Kraków',762500);

INSERT INTO state (name, population,capital_id) VALUES
('Czechy',10627448,1),
('Francja', 66259012,2),
('Litwa',3505738,3),
('Niemcy',80996685,4),
('Polska',38346279,5);

SELECT s.name as state, c.name capitol FROM state s LEFT JOIN city c ON s.capital_id = c.id;


SELECT c.name capitol FROM state s LEFT JOIN city c ON s.capital_id = c.id;
SELECT c.name AS capitol FROM state s RIGHT JOIN city c ON c.id = s.capital_id WHERE s.name IS NOT NULL;

SELECT name FROM city WHERE EXISTS (SELECT * FROM state WHERE state.capital_id = city.id);
SELECT name FROM city WHERE id = ANY (SELECT capital_id FROM state);


SELECT s.name as state, c.citizens/s.population*100 capitol FROM state s LEFT JOIN city c ON s.capital_id = c.id;


SELECT s.name as state FROM state s LEFT JOIN city c ON s.capital_id = c.id WHERE c.citizens/s.population > 0.1;

ALTER TABLE city ADD COLUMN is_capital BOOLEAN DEFAULT 0;

SET SQL_SAFE_UPDATES = 0;

UPDATE city SET is_capital = 1 WHERE EXISTS (SELECT * FROM state WHERE state.capital_id = city.id);

UPDATE city SET is_capital = EXISTS (SELECT * FROM state WHERE state.capital_id = city.id);

SELECT * FROM city;

DELETE FROM city WHERE is_capital = 0;


SELECT * FROM city LEFT JOIN state ON city.id = state.capital_id;