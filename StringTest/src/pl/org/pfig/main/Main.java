package pl.org.pfig.main;

import java.util.Random;

public class Main {
	public static void main(String[] args) {
		
		
		
		StringUtil su = new StringUtil("przykladowy tekst");
		
		su.print().prepend("__").print();
		
		String str2 = "qwertty";
		
		String[] arr = str2.split("");
		String ret = "";
		System.exit(0);
		
		
		String myStr = "  przykladowy ciag znakow  ";
		
		if(myStr.equals("  przykladowy ciag znakow  ")) {
			System.out.println("Ci�gi s� takie same.");
		}
		
		if(myStr.equalsIgnoreCase("  PRZYKLADOWY ciag znaKOW  ")) {
			System.out.println("Ci�gi znak�w s� takie same, bez badania wielko�ci liter.");
		}
		
		System.out.println("dlugosc myStr to: " + myStr.length());
		
		
		/*
		 * 	P  A  W  E  L
		 * 0  1  2  3  4
		 */
		
		System.out.println(myStr.substring(14));
		
		String otherStr = "PAWEL";
		
		System.out.println(otherStr.substring(1));
		
		System.out.println(otherStr.substring(1, 4));
		
		System.out.println(otherStr.substring(1, otherStr.length() - 1));
		
		System.out.println(myStr.trim());
		
		// myStr = myStr.trim();
		
		System.out.println(myStr.charAt(2) + "" +  myStr.charAt(3));
		
		String alphabet = "";
		for(char c = 97; c <= 122; c++) {
			alphabet += c;
		}
		
		System.out.println(alphabet);
		
		System.out.println( myStr.replace("ciag", "lancuch"));
		
		System.out.println(myStr.concat(otherStr));
		
		System.out.println(myStr);
		
		if(myStr.contains("klad")) {
			System.out.println("Slowo klad zawiera sie w " + myStr);
		}
		
		if(alphabet.startsWith("a")) {
			System.out.println("alfabet zaczyna sie od A");
		}
		
		if(alphabet.endsWith("z")) {
			System.out.println("alfabet konczy sie na Z");
		}
		
		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));
		
		String simpleStr = "pawel poszedl do lasu";
		String[] arrOfStr = simpleStr.split(" ");
		for(String s: arrOfStr) {
			System.out.println("\t" + s);
		}
		
		String s = "oksymoron";
		String firstLetter = "" + s.charAt(0);
		System.out.println(firstLetter + s.substring(1)
											.replace("o", "_"));
		
		
		
		System.out.println(s.toUpperCase());
		
		String newS = "JaKiS CiAg";
		
		System.out.println(newS.toLowerCase());
		
		String[] w = Main.checkWordsLength(3, "kiedys wybiore sie do lasu");
		
		for(String str: w) {
			if(str != null) {
				System.out.println(str);
			}
		}
		
		Random r = new Random();
		// jezeli nie przekazujemy parametru, moze zostac zwrocona liczba ujemna
		System.out.println(r.nextInt()); 
		// jezeli przekazujemy argument, zostanie zwrocona liczba z zakresu <0, liczba)
		System.out.println(r.nextInt(15)); // <0, 15)
		
		// generowanie losowej liczby z zakesu
		// <10, 25) 25 - 10 = 15
		int a = 10;
		int b = 25;
		
		System.out.println(a + r.nextInt(b - a));
		
		char c = (char) (97 + r.nextInt(26));
		System.out.println(c + "");
		
		HTMLExercise he = new HTMLExercise("To jest m�j tekst.");
		
		he.print().strong().print().p().print();
		
	}
	
	public static String[] checkWordsLength(int n, String s) {
		String[] words = s.split(" ");
		String[] ret = new String[words.length];
		int i = 0;
		for(String word : words) {
			if(word.length() > n) {
				ret[i++] = word;
			}
		}
		return ret;
	}
}
