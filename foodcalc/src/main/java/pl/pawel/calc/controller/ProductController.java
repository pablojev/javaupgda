package pl.pawel.calc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.pawel.calc.entity.Product;
import pl.pawel.calc.repository.ProductRepository;

import java.util.List;

/**
 * Created by pablojev on 28.07.2017.
 */
@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @RequestMapping("/show")
    public List<Product> showAll() {
        return (List<Product>) productRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Product showById(@PathVariable("id") String id) {
        return productRepository.findOne(Long.valueOf(id));
    }

    @RequestMapping("/add")
    public Product add(@RequestParam("name") String name,
                       @RequestParam("carbon") String carbon) {
        Product p = new Product(name, carbon);
        return productRepository.save(p);
    }
}
