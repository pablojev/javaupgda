package pl.pawel.calc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.pawel.calc.entity.Meal;
import pl.pawel.calc.entity.Product;
import pl.pawel.calc.repository.MealRepository;
import pl.pawel.calc.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablojev on 28.07.2017.
 */
@CrossOrigin
@RequestMapping("/meal")
@RestController
public class MealController {
    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping("/show")
    public List<Meal> showAll() {
        return (List<Meal>) mealRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Meal showById(@PathVariable("id") String id) {
        return mealRepository.findOne(Long.valueOf(id));
    }

    @RequestMapping("/add")
    public Meal add(@RequestParam("name") String name) {
        Meal m = new Meal(name);
        List<Product> products = new ArrayList<>();
        products.add(productRepository.findOne(1L));
        products.add(productRepository.findOne(2L));
        m.setProducts(products);
        return mealRepository.save(m);
    }

    @RequestMapping("/add/{id}/product/{productId}")
    public Meal addProductToMeal(@PathVariable("id") String id,
                                 @PathVariable("productId") String productId) {
        long mealId = Long.valueOf(id);
        long prodId = Long.valueOf(productId);

        return null;
    }
}
