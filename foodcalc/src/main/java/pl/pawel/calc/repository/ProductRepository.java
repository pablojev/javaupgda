package pl.pawel.calc.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pawel.calc.entity.Product;

/**
 * Created by pablojev on 28.07.2017.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
}
