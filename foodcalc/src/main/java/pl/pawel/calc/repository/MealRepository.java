package pl.pawel.calc.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pawel.calc.entity.Meal;

/**
 * Created by pablojev on 28.07.2017.
 */
public interface MealRepository extends CrudRepository<Meal, Long> {
}
