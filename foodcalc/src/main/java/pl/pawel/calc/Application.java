package pl.pawel.calc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.text.ParseException;

/**
 * Created by pablojev on 28.07.2017.
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) throws ParseException {
        SpringApplication.run(Application.class, args);
    }
}
