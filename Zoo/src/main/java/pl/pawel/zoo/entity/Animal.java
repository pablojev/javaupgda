package pl.pawel.zoo.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by pablojev on 12.07.2017.
 */
@Entity
@Table(name="animal")
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AnimalFeed> animalFeedSet = new LinkedList<AnimalFeed>();

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    public Animal(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AnimalFeed> getAnimalFeedSet() {
        return animalFeedSet;
    }

    public void setAnimalFeedSet(List<AnimalFeed> animalFeedSet) {
        this.animalFeedSet = animalFeedSet;
    }
}
