package pl.pawel.zoo;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.pawel.zoo.dao.AnimalDAO;
import pl.pawel.zoo.dao.AnimalFeedDAO;
import pl.pawel.zoo.entity.Animal;
import pl.pawel.zoo.entity.AnimalFeed;
import pl.pawel.zoo.util.HibernateUtil;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

/**
 * Created by pablojev on 12.07.2017.
 */
public class Main {
    public static void main(String[] args) {

        AnimalFeedDAO animalFeedDAO = new AnimalFeedDAO();

        Animal a = new Animal();
        a.setName("Słoń");

        AnimalFeed af = new AnimalFeed("Paweł", 8, a);

        animalFeedDAO.insert(af);
        animalFeedDAO.insert(new AnimalFeed("Karolina", 3, new Animal("Krokodyl")));
        animalFeedDAO.insert(new AnimalFeed("Michał", 5, a));


        System.out.println("Nakarmiono: " + animalFeedDAO.get(1).getAnimal().getName());

/*
        // BEGIN: insert
        Animal a = new Animal();
        a.setName("Słoń");

        animalDAO.insert(a);
        animalDAO.insert(new Animal("Orzeł"));
        animalDAO.insert(new Animal("Gołąb"));

        // BEGIN: retrieve
        Animal slon = new Animal();
        slon = animalDAO.get(1);

        System.out.println("Pobrales id = " + slon.getId() + " " + slon.getName());

        // BEGIN: update
        slon.setName("PrzerobionySlon");
        animalDAO.update(slon);

        // BEGIN: delete
        animalDAO.delete(3); // delete: gołąb

        Animal secondAnimal = new Animal();
        secondAnimal.setId(2);
        animalDAO.delete(secondAnimal); // delete: orzeł

        // BEGIN: retrieve all
        for(Animal anim : animalDAO.get()) {
            System.out.println(anim.getId() + ". " + anim.getName());
        }
*/
    }
}