package students;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements IFile {

	private final static String FILE_NAME = "students_text.txt";
	
	@Override
	public void save(List<Student> studentsList) {
		try (PrintStream printStream = new PrintStream(FILE_NAME)){
			for(Student student : studentsList) {
				printStream.println(student.getIndexNumber() + " " + student.getName() + " " + student.getSurname());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Student> load() {
		 
		List<Student> students = new ArrayList<>();
		try (Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream(FILE_NAME)));){
			
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] data = line.split(" ");
				Student student = new Student(Integer.parseInt(data[0]), data[1], data[2]);
				students.add(student);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return students;
	}

}
