package students;

import java.util.ArrayList;
import java.util.List;

public class FileTest {

	public static void main(String[] args) {
		
		List<Student> students = new ArrayList<>();
		students.add(new Student(100, "Jan", "Kowalski"));
		students.add(new Student(101, "Jan1", "Kowalski1"));
		University u = new University(students);

		IFile file = new BinaryFile();
		file.save(u.getStudentsList());
		
		List<Student> studentsFromFile = file.load();
		University universityFromFile = new University(studentsFromFile);
		
		universityFromFile.showAll();
		
		
		
		
	}
	
}
