package files;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileLockInterruptionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlParserExample {

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new File("src/main/resources/files/staff.xml"));
		
		doc.getDocumentElement().normalize();
		
		NodeList nodeList = doc.getElementsByTagName("staff");
		for(int i =0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				System.out.println(element.getElementsByTagName("salary").item(0).getTextContent());
			}
		}
	}

}
