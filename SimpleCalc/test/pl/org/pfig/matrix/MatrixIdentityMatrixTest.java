package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIdentityMatrixTest {

	Matrix m;
	
	@Before
	public void init() {
		m = new Matrix();
	}
	
	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };		
		assertArrayEquals(expected, m.identityMatrix(actual));
	}
	
	@Test 
	public void whenEmptyArrayGivenIdentityMatrixExpectedByValue() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		actual = m.identityMatrix(actual);
		
		for(int i = 0; i < actual.length; i++) {
			for(int j = 0; j < actual[i].length; j++) {
				assertEquals(expected[i][j], actual[i][j]);
			}
		}
	}
	
	@Test
	public void whenEmptyArrayGivenCheckDimensionsOfArray() {
		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		actual = m.identityMatrix(actual);
		
		assertTrue("First dimension value fail", actual.length == expected.length);
		
		for(int i = 0; i < actual.length; i++) {
			assertTrue("Values are not equal", actual[i].length == expected[i].length);
		}
	}

}
